<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AddNewEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index($locale)
    // {
    //     App::setLocale($locale);
    //     session()->put('locale', $locale);
    //     return redirect()->back();
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Department $department,Position $position)
    {
        return view('livewire.department.add-new-employee.create',['department'=>$department,'position'=>$position]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Department $department,Position $position)
    {
        $request->validate([
            // 'user_id'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'gender'=>'required',
            'address'=>'required',
            'phone_number'=>'required',
            'cell_phone_number'=>'required',
            'email'=>'required',
            'dob'=>'required',
            'learning_institute'=>'required',
            'marital_status'=>'required',
            'description'=>'required',
            // 'Spouses_name',
            // 'Spouses_contact',
            'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        $newImagename=time(). '-'.$request->name.'.'.$request->image->extension();
        $request->image->move(public_path('Employee'),$newImagename);
        //    department::create($request->all());
        Employee::create([
            'user_id'=>$request->input('user_id'),
            'position_id'=>$request->input('position_id'),
            'name'=>$request->input('name'),
            'gender'=>$request->input('gender'),
            'address'=>$request->input('address'),
            'phone_number'=>$request->input('phone_number'),
            'cell_phone_number'=>$request->input('cell_phone_number'),
            'email'=>$request->input('email'),
            'dob'=>$request->input('dob'),
            'learning_institute'=>$request->input('learning_institute'),
            'marital_status'=>$request->input('marital_status'),
            'description'=>$request->input('description'),
            'Spouses_name'=>$request->input('Spouses_name'),
            'Spouses_contact'=>$request->input('Spouses_contact'),
            'image_path'=>$newImagename
        ]);
            return redirect()->route('department.position.show',[$department->id,$position->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department,Position $position,Employee $employee)
    {
        // dd($employee->id);
        return view('livewire.department.add-new-employee.show',['department'=>$department,'position'=>$position,'employee'=>$employee]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department,Position $position,Employee $employee)
    {
        return view('livewire.department.add-new-employee.edit',['department'=>$department,'position'=>$position,'employee'=>$employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Department $department,Position $position,Employee $employee)
    {
        $request->validate([
            // 'user_id'=>'required',
            'position_id'=>'required',
            'name'=>'required',
            'gender'=>'required',
            'address'=>'required',
            'phone_number'=>'required',
            'cell_phone_number'=>'required',
            'email'=>'required',
            'dob'=>'required',
            'learning_institute'=>'required',
            'marital_status'=>'required',
            'description'=>'required',
            // 'Spouses_name',
            // 'Spouses_contact',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->name.'.'.$request->image->extension();
        // $request->image->move(public_path('Employee'),$newImagename);
        //    department::create($request->all());
        $input = $request->all();
        if ($image = $request->image) {
            // $destinationPath = 'image/';
            $newImagename = time(). '-'.$request->name.'.'.$request->image->extension();
            // $image->move($destinationPath, $profileImage);
            $input['image_path'] = $newImagename;
            $image->move(public_path('Employee'),$newImagename);
        }else{
            unset($input['image_path']);
        }
        $employee->update($input);
        return redirect('/admin/department/'.$department->id.'/position'.'/'.$position->id.'/show');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department,Position $position,Employee $employee)
    {
        $employee->delete();
        return redirect()->back()->with('department',$department,'position',$position,'employee',$employee);
    }
}
