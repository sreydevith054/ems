<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Console\Input\Input;
use Illuminate\Http\RedirectResponse;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $department = Department::all();
        return view('livewire.admin.department',['department'=>$department]);
     }
    // public function index1(Department $department)
    // {

    //     // $department->all();
    //     // dd($department->position());
    //     // dd($department);
    //     // dd($department->all());
    //     $positions=Position::where('department_id','=',$department->id)->get();
    //     return view('livewire.admin.all',['department'=>$department,'position'=>$positions]);
    // }

    // public function index()
    // {
        // $department=Department::orderBy('created_at','desc')->paginate(2);
            // $department=Department::all();
            // return view('test')->with('department',$department);
    //     return view('livewire.admin.department')->with('department',$department);
    // }
    // public function index2(Department $department)
    // {
    //     return view('include.main-sidebar')->with('department',$department);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $department=Department::all();
        return view('livewire.department.create',['department'=>$department]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'department' => 'required',
            'description' => 'required',
            'phone_num' => 'required',
            'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        $request->image->move(public_path('image'),$newImagename);
        //    department::create($request->all());
        department::create([
            'department' =>$request->input('department'),
            'description' => $request->input('description'),
            'phone_num' => $request->input('phone_num'),
            'image_path'=>$newImagename
        ]);

            return redirect()->route('department.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return view('livewire.department.show',['department'=>$department]);
    }

    //tab in department
    public function hometab(Department $department)
    {
            $position=$department->position()->paginate(3);

        return view('livewire.department.show',['department'=>$department,'position'=>$position]);
    }
    public function employeetab(Department $department)
    {
            $position=$department->position()->paginate(3);

        return view('livewire.department.show',['department'=>$department,'position'=>$position]);
    }
    public function positiontab(Department $department)
    {
            $position=$department->position()->paginate(3);
        return view('livewire.department.show',['department'=>$department,'position'=>$position]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
            return view('livewire.department.edit',['department'=>$department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $request->validate([
            'department' => 'required',
            'description' => 'required',
            'phone_num' => 'required',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);

        // $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        // $request->image->move(public_path('image'),$newImagename);
        // $department->update([
        //         'department' =>$request->input('department'),
        //         'description' => $request->input('description'),
        //         'phone_num' => $request->input('phone_num'),
        //         'image_path'=>$newImagename
        //     ]);
        $input = $request->all();
        if ($image = $request->image) {
            // $destinationPath = 'image/';
            $newImagename = time(). '-'.$request->department.'.'.$request->image->extension();
            // $image->move($destinationPath, $profileImage);
            $input['image_path'] = $newImagename;
            $image->move(public_path('image'),$newImagename);
        }else{
            unset($input['image_path']);
        }
        $department->update($input);
        return redirect()->route('department.index')
                        ->with('success','department updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return redirect()->route('department.index');
    }
        /**
     * @param $locale
     * @return RedirectResponse
     */

    public function change(Department $department,Position $position,Employee $employee,$locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back()->with('department',$department->id,'position',$position,'employee',$employee);
    }

}
