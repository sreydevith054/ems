<?php

namespace App\Http\Controllers;

use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AddNewPosition extends Controller
{
    public function index($locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

    public function create(Department $department)
     {
        return view('livewire.department.add-new-position.create',['departments'=>$department]);
     }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Department $departments)
    {
        $request->validate([
            'department_id' => 'required',
            'position'=> 'required',
            'description' => 'required',
            // 'phone_num' => 'required',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        // $request->image->move(public_path('image'),$newImagename);
        //    department::create($request->all());
        Position::create([
            'department_id' =>$request->input('department_id'),
            'position' => $request->input('position'),
            'description' => $request->input('description')
            // 'image_path'=>$newImagename
        ]);
        return redirect('admin/department/'.$departments->id.'/position');
    }
    public function show(Department $department,Position $position)
    {
        return view('livewire.department.add-new-position.show',['department'=>$department,'position'=>$position]);
    }

    public function edit(Department $department,Position $position)
    {
        return view('livewire.department.add-new-position.edit',['position'=>$position,'departments'=>$department]);
    }

    public function update(Request $request,Department $departments,Position $position)
    {
        $request->validate([
            'department_id' => 'required',
            'position'=> 'required',
            'description' => 'required',
            // 'phone_num' => 'required',
            // 'image'=>'required|mimes:png,jpg,jpeg|max:5048'
        ]);
        // $newImagename=time(). '-'.$request->department. '.'.$request->image->extension();
        // $request->image->move(public_path('image'),$newImagename);
        //    department::create($request->all());
        $position->update([
            'department_id' =>$request->input('department_id'),
            'position' => $request->input('position'),
            'description' => $request->input('description')
            // 'image_path'=>$newImagename
        ]);
        // return redirect()->back();
        return redirect('admin/department/'.$departments->id.'/position');
    }
    // public function destroy(Department $departments,Position $position)
    // {
    //     $test=$position->delete();
    //     return redirect()->back();
    // }



}
