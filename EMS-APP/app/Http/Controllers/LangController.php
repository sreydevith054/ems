<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Http\RedirectResponse;

class LangController extends Controller
{
    /**
     * @param $locale
     * @return RedirectResponse
     */
    public function change(Department $department,Position $position,Employee $employee,$locale)
    {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back()->with('department',$department->id,'position',$position,'employee',$employee);
    }
}
