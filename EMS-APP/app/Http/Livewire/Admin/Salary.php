<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Salary extends Component
{
    public function render()
    {
        return view('livewire.admin.salary');
    }
}
