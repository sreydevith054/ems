<?php

namespace App\Http\Livewire\User;

use Livewire\Component;

class Department extends Component
{
    public function render()
    {
        return view('livewire.user.department');
    }
}
