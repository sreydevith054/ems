<?php

namespace App\Models;

use Spatie\Searchable\SearchResult;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id',
        'position_id',
        'name',
        'gender',
        'address',
        'phone_number',
        'cell_phone_number',
        'email',
        'dob',
        'learning_institute',
        'marital_status',
        'description',
        'Spouses_name',
        'Spouses_contact',
        'image_path'
    ];
    public function position()
    {
        return $this->belongsTo(Position::class);
    }
    // public function department()
    // {
    //     return $this->hasOneThrough(department::class,position::class);
    // }

    // public function getSearchResult(): SearchResult
    // {
    //     $url = route('categories.show', $this->id);

    //     return new SearchResult(
    //         $this,
    //         $this->name,
    //         $url
    //      );
    // }
}
