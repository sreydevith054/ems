<?php

use App\Http\Controllers\AddNewEmployeeController;
use App\Http\Livewire\Crud;
use App\Http\Livewire\Admin\User;
use App\Http\Livewire\Admin\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MiniPosition;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Department;
use App\Http\Controllers\AddNewPosition;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\LangController;
use App\Http\Livewire\Admin\crud as AdminCrud;
use App\Http\Livewire\Login as LoginComponent;
use App\Http\Controllers\MiniPositionController;
use App\Http\Livewire\User\Profile as UserProfile;
use App\Http\Livewire\Dasboard as DasboardComponent;
use App\Http\Livewire\User\Dashboard as UserDashboard;
use App\Http\Middleware\LanguageManager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/', function () {
//     return redirect()->route('login');
// });
route::get('/',DasboardComponent::class)->name('dashboard');
// Route::get('/', function () {
//     return view('welcome');
// });
// Route::resource('position', PositionController::class);

// Route::resource('department', DepartmentController::class);
// Route::get('{id}/edit', [departmentController::class,'edit'])->name('edit');
// Route::resource('products', ProductController::class);
// Route::resource('posts', PostController::class);

// Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
//     return view('dashboard');
// })->name('dashboard');

// for admin

Route::middleware(['auth:sanctum', 'verified','authadmin'])->group(function () {

    Route::get('/admin/dashboard',Dashboard::class)->name('admin.dashboard');
    Route::get('/admin/profile',Profile::class)->name('admin.profile.index');
    Route::get('/admin/user',User::class)->name('admin.user');
    // Route::get('/admin/department',department::class)->name('admin.department');
    // Route::resource('/admin/department',departmentController::class)->name('admin.department');
    // Route::get('/admin/department',[DepartmentController::class,'index'])->name('admin.department');
    // Route::post('/admin/department/store',[DepartmentController::class,'store'])->name('admin.department.store');
    // Route::post('/admin/department/delete',[DepartmentController::class,'destroy'])->name('admin.department.destroy');
        Route::resource('admin/department', DepartmentController::class);
    //tab
        Route::get('admin/department/{department}/home',[DepartmentController::class,'hometab']);
        Route::get('admin/department/{department}/employee',[DepartmentController::class,'employeetab']);
        Route::get('admin/department/{department}/position',[DepartmentController::class,'positiontab']);
    //position
        Route::resource('admin/position', PositionController::class);
    //employee
        Route::resource('admin/employee', EmployeeController::class);
    //department nest postion creation
        //create
            Route::get('/admin/department/{department}/position/create',[AddNewPosition::class,'create'])->name('department.position.create');
        //store
            Route::post('/admin/department/{departments}',[AddNewPosition::class,'store'])->name('department.position.store');
        //show
            Route::get('/admin/department/{department}/position/{position}/show',[AddNewPosition::class,'show'])->name('department.position.show');
        //edit
            Route::get('/admin/department/{department}/position/{position}/edit',[AddNewPosition::class,'edit'])->name('department.position.edit');
        //update
            Route::put('admin/department/{departments}/position/{position}',[AddNewPosition::class,'update'])->name('department.position.update');
        //delete
            Route::delete('admin/department/{departments}/position/{position}',[AddNewPosition::class,'destroy'])->name('department.position.destroy');
    //department nest position nest employee creation
        //create
            Route::get('admin/department/{department}/position/{position}/show/employee/create',[AddNewEmployeeController::class,'create'])->name('department.position.employee.create');
        //store
            Route::post('admin/department/{department}/position/{position}',[AddNewEmployeeController::class,'store'])->name('department.position.employee.store');
        //show
            Route::get('admin/department/{department}/position/{position}/employee/{employee}/show',[AddNewEmployeeController::class,'show'])->name('department.position.employee.show');
        //edit
            Route::get('admin/department/{department}/position/{position}/employee/{employee}/edit',[AddNewEmployeeController::class,'edit'])->name('department.position.employee.edit');
        //update
            Route::put('admin/department/{department}/position/{position}/employee/{employee}',[AddNewEmployeeController::class,'update'])->name('department.position.employee.update');
        //delete
            Route::delete('admin/department/{department}/position/{position}/employee/{employee}',[AddNewEmployeeController::class,'destroy'])->name('department.position.employee.destroy');


            //language
            Route::get('admin/employee/lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/employee/{employee}/lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/department/lang/{locale}',[App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/position/lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/position/{position}/lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);
            Route::get('admin/department/{department}/lang/{locale}',[App\Http\Controllers\departmentController::class, 'change']);
            Route::get('admin/department/{department}/position/lang/{locale}',[App\Http\Controllers\departmentController::class, 'change']);
            Route::get('admin/department/{department}/position/{position}/lang/{locale}',[App\Http\Controllers\departmentController::class, 'change']);
            Route::get('admin/department/{department}/position/{position}/employee/{employee}/lang/{locale}',[App\Http\Controllers\departmentController::class, 'change']);
            Route::get('admin/department/{department}/position/{position}/show/employee/lang/{locale}',[App\Http\Controllers\departmentController::class, 'change']);

 });
        //Route Laguage

            // Route::get('/',[LangController::class,'index']);
            // route::get('/change',[DasboardComponent::class,'change'])->name('changelang');
            Route::get('lang/{locale}', [App\Http\Controllers\LangController::class, 'change']);

// for user

Route::middleware(['auth:sanctum', 'verified','authuser'])->group(function () {

    Route::get('/user/dashboard',UserDashboard::class)->name('user.dashboard');
    Route::get('/user/profile',UserProfile::class)->name('user.profile');
    // Route::get('/admin/post',Profile::class)->name('admin.post');
    // Route::get('/admin/user',::class)->name('admin.user');
    Route::get('user/lang/{locale}', [App\Http\Controllers\LangController::class, 'index']);

    });
