{{-- @forelse ($department->position->all() as $items )
        {{ $items['position'] }}
@empty

@endforelse ()
 --}}
 <x-base-layout>
    @section('title',__('massages.detail'))

    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ $department['department']}}({{ $department['description'] }})</div>
                {{-- @else --}}
                {{-- <div>Setting position</div> --}}
                {{-- @endif --}}
        </h1>
        <div class="row">

            <div class="col-12 col-sm-5 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.detail') }}</h4>

                  </div>
                  <div class="card-body">
                    <ul class="nav nav-pills d-flex justify-content-start" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/department/'.$department->id.'/home')? 'active show': null}}" id="home-tab1"  href="{{ url('admin/department/'.$department->id.'/home') }}" role="tab" aria-controls="home" aria-selected="true">{{ __('massages.Home') }}</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/department/'.$department->id.'/position')? 'active show':null }}" id="position-tab2"  href="{{ url('admin/department/'.$department->id.'/position') }}" role="tab" aria-controls="position" aria-selected="false">{{ __('massages.All Position') }}</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/department/'.$department->id.'/employee')? 'active show':null }}" id="employee-tab3"  href="{{ url('admin/department/'.$department->id.'/employee') }}" role="tab" aria-controls="employee" aria-selected="false">{{ __('massages.All Members') }}</a>
                      </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade {{ request()->is('admin/department/'.$department->id.'/home')? 'active show':null }}" id="{{ url('admin/department/'.$department->id.'/home') }}" role="tabpanel" aria-labelledby="home-tab1">
                          {{-- home --}}
                        @include('livewire.include.home')
                    </div>
                      <div class="tab-pane fade {{ request()->is('admin/department/'.$department->id.'/position')? 'active show':null }}" id="{{ url('admin/department/'.$department->id.'/position') }}" role="tabpanel" aria-labelledby="position-tab2">
                          {{-- position --}}
                          @include('livewire.include.position')
                        </div>
                      <div class="tab-pane fade {{ request()->is('admin/department/'.$department->id.'/employee')? 'active show':null }}" id="employee" role="tabpanel" aria-labelledby="employee-tab3">
                          @include('livewire.include.employee')
                      </div>
                    </div>
                    <a class="btn btn-danger btn-action" href="{{ url('admin/department') }}">{{ __('massages.Back') }}</a>

                  </div>

                </div>
              </div>


        </div>

    </section>
</x-base-layout>
