<x-base-layout>
                @section('title',$position['position'])
                <section class="section">
                    <h1 class="section-header">
                        <div style="font-family: 'Noto Serif Khmer', serif;">{{ $position['position'] }} ({{ $position['description'] }})</div>
                    </h1>
            <div class="row">
            <div class="col-md-9">
                <div class="section-body">
                    <div class="row mt-3">
                      <div class="col-12">
                        <div class="card">
                          <div class="card-header">

                            <div class="float-right">
                                {{-- SEARCH FUNCTION --}}
                            </div>
                            <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Employee List') }}</h4>
                            <a href="{{ route('department.position.employee.create',[$department->id,$position->id])}}" class="btn btn-sm btn-primary ion-plus-circled"></a>
                          </div>

                          <div class="card-body">
                            <div class="table-responsive">
                              <div class="d-flex justify-content-end">
                              </div>
                              <table class="table table-striped">
                                <tbody>
                                  <tr>
                                    <th>{{ __('massages.ID') }}​​</th>
                                    <th>{{ __('massages.Profile') }}​​</th>
                                    <th>{{ __('massages.Name') }}</th>
                                    <th>{{ __('massages.Position') }}</th>
                                    {{-- <th>{{ __('massages.Department') }}</th> --}}
                                    {{-- <th>Description</th> --}}
                                    {{-- <th>Status</th> --}}
                                    <th>{{ __('massages.Contact') }}</th>
                                    <th>{{ __('massages.Action') }}</th>

                                </tr>
                                @foreach($position->employee as $employees)
                                <tr>
                                  <td>{{ $employees['id'] }}</td>
                                  <td>
                                      <div class="user-image mb-3 text-center">

                                          <div style="width: 100px; height: 100px; overflow: hidden; background: #cccccc; margin: 0 auto">
                                              <img src="{{ URL::to('Employee/'.$employees['image_path']) }}" class="figure-img img-thumbnail"  id="imgPlaceholder" alt="">
                                          </div>

                                    </div>

                                    </td>

                                  <td>
                                      {{ $employees['name'] }}
                                  </td>
                                  <td>
                                      {{ $employees['position']->position}}
                                  </td>
                                  {{-- <td>{{ $employees['position']->department['department'] }}</td> --}}


                                  <td>
                                      {{$employees['email'] }}
                                      <br>
                                      {{$employees['cell_phone_number']}}
                                  </td>
                                  <td>
                                      <form action="{{ route('department.position.employee.destroy',[$department->id,$position->id,$employees->id]) }}" method="POST">
                                          <a href=" {{route('department.position.employee.show',[$department->id,$position->id,$employees->id])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.view') }}"><i class="fas fa-eye"></i></a>
                                          <a href="{{ route('department.position.employee.edit',[$department->id,$position->id,$employees->id]) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Edit') }}"><i class="ion ion-edit"></i></a>

                                          @csrf
                                          @method('DELETE')
                                       <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Delete') }}"><i class="ion ion-trash-b"></i></button>
                                      </form>
                                  </td>

                                </tr>
                                  @endforeach
                              </tbody>
                          </table>
                            </div>
                              {{-- <div class="d-flex justify-content-end">
                                   {!! $employee->links() !!}
                              </div> --}}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
            <div class="col-md-3">
                <div class="card card-sm bg-danger">
                    <div class="card-icon">
                    <i class="far fa-building"></i>
                    </div>
                    <div class="card-wrap">
                    <div class="card-body">
                        {{ $position->department->department}}
                    </div>
                    <div class="card-header">
                        <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Department') }}</h4>
                    </div>
                    </div>
                </div>
                <div class="card card-sm bg-success">
                    <div class="card-icon">
                        <i class="fas fa-users"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-body">
                            {{ $position->employee->count() }}
                        </div>
                        <div class="card-header">
                        <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Employee') }}</h4>
                        </div>
                    </div>
                    </div>
                    <div class="card card-sm bg-info">
                        <div class="card-icon">
                        <i class="ion-ios-telephone"></i>
                        </div>
                        <div class="card-wrap">
                        <div class="card-body">
                            {{ $position['department']->phone_num }}
                        </div>
                        <div class="card-header">
                            <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Contact') }}</h4>
                        </div>
                        </div>
                    </div>
            </div>
    </section>
</x-base-layout>
