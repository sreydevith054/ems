<x-base-layout>
    @section('title',__('massages.Update').' '.$position['position'].'|'.$departments['department'])
    @push('css')
    <style>
    input[type=text]:disabled{
        background: white;
      }
      textarea[type=text]:disabled{
        background: white;
      }
    </style>
    @endpush

    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Update') }} ({{ $position['position'].')|'.$departments['department'] }}</div>
                {{-- @else --}}
                {{-- <div>Setting position</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('department.position.update',[$departments->id,$position['id']]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
         <div class="row">
         <div class="col-md-6">
             {{-- header --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>{{ __('massages.Department') }}:</strong>
                        <select class="form-control" name="department_id">
                        <option value="{{ $departments['id']}}">{{ $departments['department'] }}</option>
                        </select>
                    </div>
            </div>

                {{-- section1 --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('massages.Position') }}:</strong>
                            <input type="text" value="{{ $position['position'] }}" name="position" class="form-control" placeholder="Position">
                        </div>
            </div>
                {{-- section2 --}}

            <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('massages.Description') }}</strong>
                            <textarea class="form-control" rows="1" name="description" placeholder="Description">{{ $position->description }}</textarea>
                        </div>
            </div>
                {{-- section3--}}

            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-sm float-right">{{ __('massages.Update') }}</button>
                            <a href="{{ url('admin/department/'.$departments->id.'/position') }}" class="btn btn-danger  btn-sm" role="button">{{ __('massages.Back') }}</a>
                        </div>
            </div>

         </div>

         <div class="col-md-6">

                <div class="user-image mb-3 text-center">
                        {{-- <img
                        src="
                        https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXzJJqUySdl_KDDEnRhaLd4GmDGO6acFq4nw&usqp=CAU
                        " class="default figure-img img-fluid rounded box" style="" width="400px" height="400px"> --}}
                        {{-- @foreach ($departments as $department) --}}
                            <img
                            src="
                            {{ asset('image/'.$departments['image_path'])}}
                            " class="{{ $departments['id'] }} figure-img img-fluid rounded box" width="400px" height="400px">
                        {{-- @endforeach --}}
                </div>
         </div>
</form>
<div class="col-md-12 col-lg-12 col-sm-12">
    <div class="row">
        {{-- <div class="col-md-6 ml-4">
            <strong>Type of Position:</strong>
            <textarea type='text' cols="170" disabled class="form-control" rows="2">@foreach ($departments->position as $position){{$position->position}},@endforeach
            </textarea>
        </div> --}}
            <div class="col-md-6 ml-2">
              <div class="card">
                <div class="card-header">
                  <div class="float-right">
                    <a data-collapse="#mycard-collapse" class="btn btn-icon"><i class="ion ion-minus"></i></a>
                  </div>
                  <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.PREVIEWS') }} {{ __('massages.All Position') }} </h4>
                </div>
                    <div class="collapse show" id="mycard-collapse">
                        <div class="card-body">
                            <textarea type='text' cols="170" disabled class="form-control" rows="2">@foreach ($departments->position as $position){{$position->position}},@endforeach
                            </textarea>
                        </div>
                    </div>
        </div>
    </div>
    </div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });
    $(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
@endpush
</x-base-layout>
