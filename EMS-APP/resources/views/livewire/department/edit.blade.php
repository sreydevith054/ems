<x-base-layout>
    @section('title',__('massages.Update').' '.$department['department'].__('massages.department'))

    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Update').' '.$department['department'].' '.__('massages.department') }}</div>
                {{-- @else --}}
                {{-- <div>Setting Department</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('department.update',$department['id']) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
         <div class="col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Department') }}:</strong>
                    <input type="text" value="{{ $department['department'] }}" name="department" class="form-control" placeholder="Department">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Description') }}</strong>
                    <textarea class="form-control" rows="1" name="description" placeholder="Description">{{ $department['description'] }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.PhoneNumber') }}:</strong>
                    <input type="tel" value="{{ $department['phone_num'] }}" name="phone_num" class="form-control" placeholder="+855">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <strong>{{ __('massages.Image') }}:</strong>
                <div class="custom-file">
                    <input type="file" name="image"  class="custom-file-input" id="chooseFile">
                    <label class="custom-file-label" for="chooseFile">{{ $department['image_path'] }}</label>
                </div>
                </div>
            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm float-right">{{ __('massages.Update') }}</button>
                <a href="{{ route('department.index') }}" class="btn btn-danger  btn-sm" role="button">{{ __('massages.Back') }}</a>
            </div>
        </div>

         </div>

         <div class="col-md-6">
        {{-- <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" placeholder="Department">
            </div>
        </div> --}}

        <div class="user-image mb-3 text-center">
            {{-- <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto"> --}}
              <img src="{{ URL::to('image/',$department['image_path'])}}" width="500px" height="500px" class="figure-img img-fluid rounded"  id="imgPlaceholder" alt="">
            {{-- </div> --}}
          </div>
        </div>

</form>
</div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });
</script>
@endpush
</x-base-layout>
