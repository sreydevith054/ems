
    <div class="section-body">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <div class="d-flex justify-content-end">
                </div>
                <table class="table table-striped">
                  <tbody>
                    <tr>
                        <th>{{ __('massages.ID') }}​​</th>
                        <th>{{ __('massages.Profile') }}​​</th>
                        <th>{{ __('massages.Name') }}</th>
                        <th>{{ __('massages.Position') }}</th>
                        <th>{{ __('massages.Department') }}</th>
                        {{-- <th>Description</th> --}}
                        {{-- <th>Status</th> --}}
                        <th>{{ __('massages.Contact') }}</th>

                {{-- <th>Action</th> --}}

                  </tr>
                  @foreach($department['employee'] as $employees)
                  <tr>
                    <td>{{ $employees['id'] }}</td>
                    <td>
                        <div class="user-image mb-3 text-center">

                            <div style="width: 100px; height: 100px; overflow: hidden; background: #cccccc; margin: 0 auto">
                                <img src="{{ URL::to('Employee/'.$employees['image_path']) }}" class="figure-img img-thumbnail"  id="imgPlaceholder" alt="">
                            </div>

                      </div>

                      </td>

                    <td>
                        {{ $employees['name'] }}
                    </td>
                    <td>
                        {{ $employees['position']->position}}
                    </td>
                    <td>{{ $employees['position']->department['department'] }}</td>


                    <td>
                        {{$employees['email'] }}
                        <br>
                        {{$employees['cell_phone_number']}}
                    </td>
                    {{-- <td>
                        <form action="{{ route('employee.destroy',$employees->id) }}" method="POST">
                            <a href=" {{route('employee.show',$employees['id'])}}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="view"><i class="fas fa-eye"></i></a>
                            <a href="{{ route('employee.edit',$employees['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ion ion-edit"></i></a>

                            @csrf
                            @method('DELETE')
                         <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="Delete"><i class="ion ion-trash-b"></i></button>
                        </form>
                    </td> --}}

                  </tr>
                    @endforeach
                </tbody>
            </table>
              </div>
                <div class="d-flex justify-content-end">
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
