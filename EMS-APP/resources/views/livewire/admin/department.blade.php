<x-base-layout>
<div>
    @section('title', __('massages.All Department'))

    <section class="section">
        @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <p class="text-sm">{{ session('message') }}</p>
                    </div>
                </div>
            </div>
        @endif
            <h1 class="section-header">
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.All Department') }}</div>
        </h1>
        <div class="section-body">
          <div class="row mt-5">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                  {{-- <div class="float-right">
                    <form action="">
                      <div class="input-group">
                        <input type="text" id="myInput" name="search" value="" class="form-control" placeholder="{{ __('massages.Search') }}">
                        <div class="input-group-btn">
                          <button type="submit" id="myBtn" class="btn btn-secondary"><i class="ion ion-search"></i></button>
                        </div>
                      </div>
                    </form>

                  </div> --}}
                  <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.DEPARTMENT') }}</h4>
                  <a href="{{ route('department.create') }}" class="btn btn-sm btn-primary ion-plus-circled"></a>

                </div>

                <div class="card-body">

                  <div class="table-responsive">
                    <div class="d-flex justify-content-end">
                    </div>

                    {{-- <table id="example" class="table table-striped">
                      <tbody>
                          <tr>
                        <th>{{ __('massages.Picture') }}</th>
                        <th>{{ __('massages.Department') }}</th>
                        <th>{{ __('massages.Members') }}</th>
                        <th>{{ __('massages.Position') }}</th>
                        <th>{{ __('massages.Action') }}</th>
                      </tr>
                      @foreach($department as $departments)
                      <tr>
                        <td>
                            <img alt="image" src="{{ asset('image/'.$departments['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="Wildan Ahdian">
                        </td>

                        <td>
                            {{ $departments['department'] }}
                        </td>
                        <td>
                            @forelse ($departments->employee as $item )
                            <img alt="image" src="{{ asset('Employee/'.$item['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="{{ $item->name }}">
                            @empty
                            none
                            @endforelse
                            </div>
                        </td>
                        <td>
                            <div class="badge badge-info">
                                {{ $departments->position->count() }}
                            </div>

                        </td>
                        <td>
                            <form action="{{ route('department.destroy',$departments->id) }}" method="POST">
                                <a href=" {{ url('admin/department/'.$departments->id.'/home') }}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.view') }}"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('department.edit',$departments['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Edit') }}"><i class="ion ion-edit"></i></a>
                          @csrf
                          @method('DELETE')
                     <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Delete') }}"><i class="ion ion-trash-b"></i></button>
                        </form>

                        </td>
                      </tr>
                        @endforeach
                    </tbody>
                </table> --}}
                      {{-- <div class="col-xs-12"> --}}
                    <table class="table table-bordered table-striped table-hover dt-responsive">
                          <thead>
                            <tr>
                                <th>{{ __('massages.Picture') }}</th>
                                <th>{{ __('massages.Department') }}</th>
                                <th>{{ __('massages.Members') }}</th>
                                <th>{{ __('massages.Position') }}</th>
                                <th>{{ __('massages.Action') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                        @foreach($department as $departments)
                        <tr>
                          <td>
                              <img alt="image" src="{{ asset('image/'.$departments['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="Wildan Ahdian">
                          </td>

                          <td>
                              {{ $departments['department'] }}
                          </td>
                          <td>
                              @forelse ($departments->employee as $item )
                              <img alt="image" src="{{ asset('Employee/'.$item['image_path']) }}" class="rounded-circle" width="35" data-toggle="title" title="{{ $item->name }}">
                              @empty
                              none
                              @endforelse
                              </div>
                          </td>
                          <td>
                              <div class="badge badge-info">
                                  {{ $departments->position->count() }}
                              </div>

                          </td>
                          <td>
                              <form action="{{ route('department.destroy',$departments->id) }}" method="POST">
                                  <a href=" {{ url('admin/department/'.$departments->id.'/home') }}" class="btn btn-success btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.view') }}"><i class="fas fa-eye"></i></a>
                                  <a href="{{ route('department.edit',$departments['id']) }}" class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Edit') }}"><i class="ion ion-edit"></i></a>
                            @csrf
                            @method('DELETE')
                       <button class="btn btn-danger btn-action" data-toggle="tooltip" title="" data-original-title="{{ __('massages.Delete') }}"><i class="ion ion-trash-b"></i></button>
                          </form>

                          </td>
                        </tr>
                          @endforeach
                      </tbody>
                        {{-- <tfoot>

                       </tfoot> --}}
                    </table>
                      </div>
                  {{-- </div> --}}
                        {{-- <div class="d-flex justify-content-end">
                            {!! $department->links() !!}
                        </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
</div>
    <script>
        $('table').DataTable();
    var table = $('table').DataTable();
    </script>
</x-base-layout>
