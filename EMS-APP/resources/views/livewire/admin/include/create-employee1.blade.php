<div class="row">
    <div class="col-md-6">
        <div class="col-xs-12 col-sm-12 col-md-12 d-none">
            <div class="form-group">
                <strong>Position:</strong>
                <select class="form-control" name="position_id">
                <option value="{{ $position['id'] }}">{{ $position['department']->department }} | {{ $position['position']}}</option>
                </select>
            </div>
        </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>{{ __('massages.Name') }}:</strong>
                <input type="text" name="name" class="form-control" placeholder="{{ __('massages.Name') }}">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4">
            <div class="form-group">
                <strong>{{ __('massages.Gender') }}:</strong>
                <select class="form-control" name="gender">
                <option value="default">{{ __('massages.Choose') }}</option>
                <option value="{{ __('massages.Male') }}">{{ __('massages.Male') }}</option>
                <option value="{{ __('massages.Female') }}">{{ __('massages.Female') }}</option>

                </select>
            </div>
        </div>
        </div>
    </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('massages.Date Of Birth') }}:</strong>
                <input id="date" type="date" name="dob" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('massages.Educational institution') }}</strong>
                <input type="text" name="learning_institute" class="form-control" placeholder="{{ __('massages.Educational institution') }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <div class="form-group">
                    <strong>{{ __('massages.Marital Status') }}</strong>
                    <select class="form-control" name="marital_status">
                    <option value="">{{ __('massages.Choose') }}</option>
                    <option value="{{ __('massages.Single') }}">{{ __('massages.Single') }}</option>
                    <option value="{{ __('massages.Married') }}">{{ __('massages.Married') }}</option>
                    <option value="{{ __('massages.Windowed') }}">{{ __('massages.Windowed') }}</option>
                    <option value="{{ __('massages.Divored') }}">{{ __('massages.Divorced') }}</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>{{ __("massages.Spouse's Name") }}:</strong>
                <input type="text" name="Spouses_name" class="form-control" placeholder="{{ __("massages.Spouse's Name") }} ({{ __("massages.Optional") }})">
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>{{ __("massages.Spouse's Contact") }}:</strong>
                <input type="text" name="Spouses_contact" class="form-control" placeholder="{{ __("massages.Spouse's Contact") }} ({{ __("massages.Optional") }})">
            </div>
        </div>
        </div>
            </div>


        <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
            <div class="form-group">
                <strong>{{ __("massages.Address") }}:</strong>
                <textarea class="form-control" rows="3" name="address" placeholder="{{ __("massages.Address") }}"></textarea>
            </div>
        </div>
    </div>

    <div class="col-md-6">
    <div class="row d-flex justify-content-center">
        <div class="col-xs-9 col-sm-9 col-md-9 mb-3">
            <strong>{{ __("massages.Image") }}:</strong>
            <div class="custom-file">
                <input type="file" name="image" class="custom-file-input" id="chooseFile">
                <label class="custom-file-label" for="chooseFile">{{ __("massages.Select file") }}</label>
            </div>
            </div>
    </div>


    <div class="user-image mb-3 text-center">
        <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto">
            <img src="..." class="figure-img img-fluid rounded"  id="imgPlaceholder" alt="">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row d-flex justify-content-center ">

            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <strong>{{ __("massages.Description") }}:</strong>
                    <textarea class="form-control" rows="1" name="description" placeholder="{{ __("massages.Description") }}"></textarea>
                </div>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-10">
                <div class="form-group">
                    <strong>{{ __("massages.Email") }}:</strong>
                    <input type='email' name="email" class="form-control" placeholder="{{ __("massages.Email") }}">
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
                <div class="form-group">
                    <strong>{{ __("massages.Phone Number") }}:</strong>
                    <input type="text" name="phone_number" class="form-control" placeholder="{{ __("massages.Phone Number") }}">
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-5">
                    <div class="form-group">
                        <strong>{{ __("massages.Cell Phone") }}:</strong>
                        <input type="text" name="cell_phone_number" class="form-control" placeholder="{{ __("massages.Cell Phone") }}">
                    </div>
            </div>
        </div>

    </div>
</div>


