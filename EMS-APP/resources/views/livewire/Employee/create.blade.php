<x-base-layout>
    <div>
    @section('title',__('massages.Add New Employee'))
            <section class="section">
                    <h1 class="section-header">
                        {{-- @if ($updateMode) --}}
                        <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Add New Employee') }}</div>
                       ​ {{-- @else --}}
                        {{-- <div>Setting employee</div> --}}
                        {{-- @endif --}}
                </h1>

                <div class="row">
                    <div class="col-lg-5 margin-tb">
                    </div>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <form action="{{ route('employee.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @include('livewire.admin.include.create-employee')

                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="form-group">
                                <a href="{{ route('employee.index') }}" class="btn btn-danger  btn-sm" role="button" data-toggle="tooltip" title="" data-original-title=""><i class="ion-arrow-left-a"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="form-group d-flex justify-content-end">
                                <button type="Submit" class="btn btn-primary btn-sm float-right mr-5" role="button" data-toggle="tooltip" title="" data-original-title=""><i class="ion-checkmark-circled"></i></button>
                            </div>
                        </div>

                    </form>
                </div>
            </section>
        </div>
        @push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });
</script>
@endpush

</x-base-layout>
