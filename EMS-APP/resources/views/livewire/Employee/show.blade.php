<x-base-layout>
    @section('title','View Employee')
    @push('css')
    <style>
    input[type=text]:disabled{
        background: white;
      }
      textarea[type=text]:disabled{
        background: white;
      }
    </style>
    @endpush
    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Employee') }} ({{ $employee->position->department->department }})</div>
                {{-- @else --}}
                {{-- <div>Setting employee</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="row">
         <div class="col-md-6">
            {{-- header --}}
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Department:</strong>
                    <input disabled type="text" placeholder="{{ $employee->position->department->department }}" name="name" class="form-control">
                </div>
            </div> --}}

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Position') }}:</strong>
                    <input disabled type="text" value="{{ $employee->position->position }}" name="name" class="form-control" placeholder="Name">
                    </select>
                </div>
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8">
                <div class="form-group">
                    <strong>{{ __('massages.Name') }}:</strong>
                    <input disabled type="text" value="{{ $employee['name'] }}" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">
                <div class="form-group">
                    <strong>{{ __('massages.Gender') }}:</strong>
                    <input disabled type="text" value="{{ $employee['gender'] }}" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            </div>
        </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Date Of Birth') }}:</strong>
                    <input disabled id="date" type="text" data-date="" data-date-format="DD/MM/YYYY" value="{{ date("d/m/Y",strtotime($employee['dob'])) }}" name="dob" class="form-control">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Educational institution') }}:</strong>
                    <input disabled type="text" value="{{ $employee['learning_institute'] }}" name="learning_institute" class="form-control" placeholder="Educational Institution">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <div class="form-group">
                        <strong>{{ __('massages.Marital Status') }}</strong>
                        <input disabled type="text" value="{{ $employee['marital_status'] }}" name="learning_institute" class="form-control" placeholder="Educational Institution">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                      <strong>{{ __("massages.Spouse's Name") }}:</strong>
                      <input disabled type="text" value="{{ $employee['Spouses_name'] }}" name="Spouses_name" class="form-control" placeholder="Spouse's Name (Optional)">
                  </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                      <strong>{{ __("massages.Spouse's Contact") }}:</strong>
                      <input disabled type="text" value="{{ $employee['Spouses_contact'] }}" name="Spouses_contact" class="form-control" placeholder="Spouse's Contact (Optional)">
                  </div>
              </div>
              </div>
                </div>


            <div class="col-xs-12 col-sm-12 col-md-12 mt-2">
                <div class="form-group">
                    <strong>{{ __("massages.Address") }}</strong>
                    <textarea disabled type='text' class="form-control" rows="2" name="address" placeholder="address">{{ $employee['address'] }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
                <a href="{{ route('employee.index') }}" class="btn btn-danger  btn-sm" role="button" data-toggle="tooltip" title="" data-original-title=""><i class="ion-arrow-left-a"></i></a>
            </div>

         </div>

         <div class="col-md-6">

        <div class="user-image mt-3 text-center">

                <div style="width: 300px; height: 300px; overflow: hidden; background: #cccccc; margin: 0 auto">
                    <img src="{{ URL::to('Employee/'.$employee['image_path']) }}" class="figure-img img-fluid rounded"  id="imgPlaceholder" alt="">
                </div>

          </div>
        <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
            <div class="row d-flex justify-content-center">

                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="form-group">
                        <strong>{{ __("massages.Description") }}:</strong>
                        <textarea disabled type='text' class="form-control" rows="2" name="description" placeholder="description">{{ $employee['description'] }}</textarea>
                    </div>
                </div>
                <div class="col-xs-10 col-sm-10 col-md-10">
                    <div class="form-group">
                        <strong>{{ __("massages.Email") }}:</strong>
                        <input disabled type='text' value="{{ $employee['email'] }}" name="email" class="form-control" placeholder="Email">
                    </div>
                </div>

                <div class="col-xs-5 col-sm-5 col-md-5 mt-2">
                    <div class="form-group">
                        <strong>{{ __("massages.Phone Number") }}:</strong>
                        <input disabled type="text" value="{{ $employee['phone_number'] }}" name="phone_number" class="form-control" placeholder="Phone Number">
                    </div>
                </div>

                <div class="col-xs-5 col-sm-5 col-md-5 mt-2">
                        <div class="form-group">
                            <strong>{{ __("massages.Cell Phone") }}:</strong>
                            <input disabled type="text" value="{{  $employee['cell_phone_number'] }}" name="cell_phone_number" class="form-control" placeholder="Cell Phone">
                        </div>
                </div>
            </div>
            <div class="form-group">
                <a href="{{ route('employee.edit',$employee['id']) }}" class="btn btn-primary btn-action float-right mr-5" data-toggle="tooltip" title="" data-original-title=""><i class="ion ion-edit"></i></a>

            </div>

        </div>
    </div>
</div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });

$("input").on("change", function() {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format( this.getAttribute("data-date-format") )
        )
    }).trigger("change")
        </script>

@endpush
</x-base-layout>
