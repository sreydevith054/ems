<x-base-layout>
    @section('title',__('massages.Add New Position'))

    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Add New Position') }}</div>
                {{-- @else --}}
                {{-- <div>Setting position</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('position.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
     <div class="row">
         <div class="col-md-6">
             {{-- header --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>{{ __('massages.Department') }}:</strong>
                        <select class="form-control" name="department_id">
                        <option value="default">{{ __('massages.Choose') }}</option>
                        @foreach ($department as $departments)
                        <option value="{{ $departments['id'] }}">{{ $departments['department'] }}</option>
                        @endforeach
                        </select>
                    </div>
            </div>

                {{-- section1 --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{  __('massages.Choose')  }}:</strong>
                            <input type="text" name="position" class="form-control" placeholder="{{  __('massages.Position')  }}">
                        </div>
            </div>
                {{-- section2 --}}

            <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{  __('massages.Description')  }}</strong>
                            <textarea class="form-control" rows="1" name="description" placeholder="{{  __('massages.Description')  }}"></textarea>
                        </div>
            </div>
                {{-- section3--}}

            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-sm float-right">{{  __('massages.Add')  }}</button>
                            <a href="{{ route('position.index') }}" class="btn btn-danger  btn-sm" role="button">{{  __('massages.Back')  }}</a>
                        </div>
            </div>

         </div>

         <div class="col-md-6">

                <div class="user-image mb-3 text-center">
                        <img
                        src="
                        https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSXzJJqUySdl_KDDEnRhaLd4GmDGO6acFq4nw&usqp=CAU
                        " class="default figure-img img-fluid rounded box" style="" width="400px" height="400px">
                        @foreach ($department as $departments)
                            <img
                            src="
                            {{ asset('image/'.$departments['image_path'])}}
                            " class="{{ $departments['id'] }} figure-img img-fluid rounded box" width="400px" height="400px">
                        @endforeach
                </div>
         </div>
</form>
</div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });
    $(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
@endpush
</x-base-layout>
