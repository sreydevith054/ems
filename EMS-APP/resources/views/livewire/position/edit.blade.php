<x-base-layout>
    @section('title',__('massages.Update').' '.__('massages.Position'))

    <section class="section">
            <h1 class="section-header">
                {{-- @if ($updateMode) --}}
                <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Update').' '.__('massages.Position') }}</div>
                {{-- @else --}}
                {{-- <div>Setting position</div> --}}
                {{-- @endif --}}
        </h1>

<div class="row">
    <div class="col-lg-5 margin-tb">
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('position.update',$position['id']) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
     <div class="row">
         <div class="col-md-6">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Department') }}:</strong>
                    <select class="form-control" name="department_id">
                    <option value="{{ $position->department->id }}" class="d-none">{{ $position->department->department }}</option>
                    @foreach ($department as $departments)
                    <option value="{{ $departments['id'] }}">{{ $departments['department'] }}</option>
                    @endforeach
                    </select>
                </div>
            </div>

            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>position:</strong>
                    <input type="text" name="position" class="form-control" placeholder="position">
                </div>
            </div> --}}

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Position') }}:</strong>
                    <input type="text" value="{{ $position['position'] }}" name="position" class="form-control" placeholder="Position">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>{{ __('massages.Description') }}</strong>
                    <textarea class="form-control" rows="1" name="description" placeholder="Description">{{ $position['description'] }}</textarea>
                </div>
            </div>
            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>PhoneNumber:</strong>
                    <input type="tel" name="phone_num" class="form-control" placeholder="+855">
                </div>
            </div> --}}

            {{-- <div class="col-xs-12 col-sm-12 col-md-12">
                <strong>Image:</strong>

                <div class="custom-file">
                    <input type="file" name="image" class="custom-file-input" id="chooseFile">
                    <label class="custom-file-label" for="chooseFile">Select file</label>
                </div>
            </div> --}}

            <div class="col-xs-12 col-sm-12 col-md-12 my-3">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm float-right">{{ __('massages.Update') }}</button>
                <a href="{{ route('position.index') }}" class="btn btn-danger  btn-sm" role="button">{{ __('massages.Back') }}</a>
            </div>
        </div>

         </div>

         <div class="col-md-6">
        {{-- <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
                <strong>Image:</strong>
                <input type="file" name="image" class="form-control" placeholder="position">
            </div>
        </div> --}}

            <div class="user-image mb-3 text-center">
                {{-- <div style="width: 400px; height: 400px; overflow: hidden; background: #cccccc; margin: 0 auto"> --}}
                    {{-- @foreach ($department as $departments)
                    @if ($departments->id==)

                    @endif
                    {{ $departments['image_path'] }}
                    @endforeach --}}
                    {{-- <img id="image" src="Null_Image.png" />
                    <select id="CarList">
                       <option value="Null_Image.png">No Car</option>
                       <option value="{{ asset('image/1636104840-IT.png') }}">Volvo</option>
                       <option value="audi.png">Audi</option>
                  </select> --}}

                <img
                class="d-none"
                src="
                {{ asset('image/'.$position->department->image_path)}}
                " class="{{ $position->department->id }} figure-img img-fluid rounded box" style="" width="500px" height="500px">

                  @foreach ($department as $departments)
                <img
                src="
                {{ asset('image/'.$departments['image_path'])}}
                " class="{{ $departments['id'] }} figure-img img-fluid rounded box" style="" width="500px" height="500px">
                {{-- </div> --}}
            @endforeach
            </div>
         </div>
</form>
</div>
</section>
@push('script')
<!-- jQuery -->
{{-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script> --}}
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $('#imgPlaceholder').attr('src', e.target.result);
        }

        // base64 string conversion
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#chooseFile").change(function () {
      readURL(this);
    });
    $(document).ready(function(){
    $("select").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".box").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else{
                $(".box").hide();
            }
        });
    }).change();
});
</script>
@endpush
</x-base-layout>
