<x-base-layout>
    @section('title',$position['position'])
    <section class="section">
        <h1 class="section-header">
            {{-- @if ($updateMode) --}}
            <div style="font-family: 'Noto Serif Khmer', serif;">{{ $position['position'] }} ({{ $position['description'] }})</div>
            {{-- @else --}}
            {{-- <div>Setting position</div> --}}
            {{-- @endif --}}
        </h1>
<div class="row">
<div class="col-md-6">
    <img
    src="
    {{ asset('image/'.$position->department['image_path'])}}"
        class=" figure-img img-fluid rounded box"
         width="800px"
         height="800px"
      >
</div>
<div class="col-md-6">
    <div class="card card-sm bg-danger">
        <div class="card-icon">
          <i class="far fa-building"></i>
        </div>
        <div class="card-wrap">
          <div class="card-body">
            {{ $position->department->department}}
          </div>
          <div class="card-header">
            <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Department') }}</h4>
          </div>
        </div>
      </div>
       <div class="card card-sm bg-success">
          <div class="card-icon">
            <i class="fas fa-users"></i>
          </div>
          <div class="card-wrap">
            <div class="card-body">
                {{ $position->employee->count() }}
            </div>
            <div class="card-header">
              <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Employee') }}</h4>
            </div>
          </div>
        </div>
        <div class="card card-sm bg-info">
            <div class="card-icon">
              <i class="ion-ios-telephone"></i>
            </div>
            <div class="card-wrap">
              <div class="card-body">
                  {{ $position['department']->phone_num }}
              </div>
              <div class="card-header">
                <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Contact') }}</h4>
              </div>
            </div>
        </div>
</div>
    </section>
</x-base-layout>
