<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('partials._head')
    @include('partials.style')
    @stack('css')
    @livewireStyles
</head>
<body>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            {{-- navbar --}}

          @include('include.navbar')

          {{-- end-navbar --}}
          {{-- main-sidebar --}}
          @if (Route::has('login'))
            @auth
            @if (Auth::user()->user_type==='admin'||Auth::user()->user_type==='user')
                @if (Route('dashboard'))
                @include('include.main-sidebar')
                @endif
            @endif
            @endauth
          @endif
        <div class='main-content'>
          {{-- main-content --}}
            {{ $slot }}
          {{-- end-main-content --}}
        {{-- </div> --}}
          {{-- <footer class="main-footer">
            <div class="footer-left">
              Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://multinity.com/">Multinity</a>
            </div>
            <div class="footer-right"></div>
          </footer> --}}
        {{-- </div>
      </div> --}}

    {{-- @yield('content') --}}
    <footer class="main-footer">
        <div class="footer-left" style="font-family: 'Noto Serif Khmer', serif;">
         {{ __('massages.Copyright') }} © 2021 <div class="bullet"></div>{{ __('massages.Develop By') }}  <a href="https://www.facebook.com/profile.php?id=100075194902754">SREY DEVITH</a>
        </div>
        <div class="footer-right"></div>
      </footer>
@include('partials._script')
@livewireScripts

@stack('script')
</body>
</html>
