<nav class="navbar navbar-expand-lg main-navbar">
    {{-- <div class="col-md-4">
        <select class="form-control "id='changelang'>
            <option value="en" {{ session()->get('locale') == 'en' ? 'selected' : '' }}>English</option>
            <option value="kh" {{ session()->get('locale') == 'kh' ? 'selected' : '' }}>Khmer</option>
        </select>
    </div> --}}

    @if (Route::has('login'))
    @auth
    @if (Auth::user()->user_type==='admin'||Auth::user()->user_type==='user')

    <form class="form-inline mr-auto">
      <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li>
        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li>
      </ul>

    </form>
    <ul class="navbar-nav navbar-right">
            {{-- <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="ion ion-ios-bell-outline"></i></a>
                <div class="dropdown-menu dropdown-list dropdown-menu-right">
                <div class="dropdown-header">Notifications
                    <div class="float-right">
                    <a href="#">View All</a>
                    </div>
                </div>
                <div class="dropdown-list-content">
                    <a href="#" class="dropdown-item dropdown-item-unread">
                    <img alt="image" src="../dist/img/avatar/avatar-1.jpeg" class="rounded-circle dropdown-item-img">
                    <div class="dropdown-item-desc">
                        <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                        <div class="time">10 Hours Ago</div>
                    </div>
                    </a>
                    <a href="#" class="dropdown-item dropdown-item-unread">
                    <img alt="image" src="../dist/img/avatar/avatar-2.jpeg" class="rounded-circle dropdown-item-img">
                    <div class="dropdown-item-desc">
                        <b>Ujang Maman</b> has moved task <b>Fix bug footer</b> to <b>Progress</b>
                        <div class="time">12 Hours Ago</div>
                    </div>
                    </a>
                    <a href="#" class="dropdown-item">
                    <img alt="image" src="../dist/img/avatar/avatar-3.jpeg" class="rounded-circle dropdown-item-img">
                    <div class="dropdown-item-desc">
                        <b>Agung Ardiansyah</b> has moved task <b>Fix bug sidebar</b> to <b>Done</b>
                        <div class="time">12 Hours Ago</div>
                    </div>
                    </a>
                    <a href="#" class="dropdown-item">
                    <img alt="image" src="../dist/img/avatar/avatar-4.jpeg" class="rounded-circle dropdown-item-img">
                    <div class="dropdown-item-desc">
                        <b>Ardian Rahardiansyah</b> has moved task <b>Fix bug navbar</b> to <b>Done</b>
                        <div class="time">16 Hours Ago</div>
                    </div>
                    </a>
                    <a href="#" class="dropdown-item">
                    <img alt="image" src="../dist/img/avatar/avatar-5.jpeg" class="rounded-circle dropdown-item-img">
                    <div class="dropdown-item-desc">
                        <b>Alfa Zulkarnain</b> has moved task <b>Add logo</b> to <b>Done</b>
                        <div class="time">Yesterday</div>
                    </div>
                    </a>
                </div>
                </div>
            </li> --}}
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
                        <i class="ion ion-android-person d-lg-none"></i>
                            <div class="d-sm-none d-lg-inline-block">{{ Auth::user()->name}}
                            </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                        @if (Auth::user()->user_type==='admin')

                        <a href="{{route('admin.profile.index')}}" class="dropdown-item has-icon">
                            <i class="ion ion-android-person"></i> {{ __('massages.Profile') }}
                        </a>
                        @elseif (Auth::user()->user_type==='user')
                        <a href="{{route('user.profile')}}" class="dropdown-item has-icon">
                            <i class="ion ion-android-person"></i> {{ __('massages.Profile') }}
                        </a>

                        @endif
                    <a href="{{ (Auth::user()->user_type==='admin')? route('admin.dashboard'):route('user.dashboard') }}" class="dropdown-item has-icon">
                        <i class="ion ion-speedometer"></i>{{ __('massages.Dashboard') }}
                    </a>
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf
                                <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                this.closest('form').submit();" class="dropdown-item has-icon">
                                    <i class="ion ion-log-out"></i> {{ __('massages.Logout') }}
                                </a>
                        </form>
                </div>
            </li>
            @php $locale = session()->get('locale'); @endphp
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    @switch($locale)
                        @case('en')
                        <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English
                        @break
                        @case('kh')
                        <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/kh.svg')}}" width="25px"> ខ្មែរ
                        @break
                        @default
                        <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English
                    @endswitch
                    <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="lang/en"><img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English</a>
                    <a class="dropdown-item" href="lang/kh"><img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/kh.svg')}}" width="25px">ខ្មែរ</a>
                </div>
            </li>
    </ul>
    @endif
    @endauth
    @endif
    @if (Route::has('login'))
        @auth
    @else
    <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
            <div class="login-brand m-0 ml-3"><a href="{{ route('dashboard') }}" class="underline text-muted">{{ __('massages.EMS') }}</a></div>
          {{-- <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li> --}}
          {{-- <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="ion ion-search"></i></a></li> --}}
        </ul>

      </form>
    <ul class="navbar-nav navbar-right">
            <li class="nav-item">
                <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline text-muted">{{ __('massages.Login') }}</a>
            </li>
        @if (Route::has('register'))
            <li class="nav-item">
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline text-muted">{{ __('massages.Register') }}</a>
            </li>
        @endif
       @php $locale = session()->get('locale'); @endphp
        <li class="nav-item dropdown ml-5">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                @switch($locale)
                    @case('en')
                    <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English
                    @break
                    @case('kh')
                    <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/kh.svg')}}" width="25px"> ខ្មែរ
                    @break
                    @default
                    <img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English
                @endswitch
                <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="lang/en"><img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/gb.svg')}}" width="25px"> English</a>
                <a class="dropdown-item" href="lang/kh"><img src="{{asset('../dist/modules/flag-icon-css/flags/4x3/kh.svg')}}" width="25px">ខ្មែរ</a>
            </div>
        </li>
    </ul>
    @endauth
    @endif
  </nav>
