<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a style="font-family: 'Noto Serif Khmer', serif;" href="{{ route('dashboard') }}">{{ __('massages.EMS') }}</a>
      </div>
      <div class="sidebar-user">
        <a href="{{ url('admin/profile') }}">
        <div class="sidebar-user-picture">
          <img alt="image" class="img-thumbnail" src="{{ asset('storage/'.Auth::user()->profile_photo_path) }}">
        </div>
        </a>
        <div class="sidebar-user-details">
          <div class="user-name">{{ Auth::user()->name}}</div>
          <div class="user-role" style="font-family: 'Noto Serif Khmer', serif;">
            {{ __('massages.'.Auth::user()->user_type) }}
          </div>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="menu-header"  style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Dashboard') }}</li>
        <li class="{{ request()->is('admin/dashboard')? 'active' : '' }}">
          <a href="{{
          (Auth::user()->user_type==='admin')?
          route('admin.dashboard'):route('user.dashboard') }}">
          <i class="ion ion-speedometer"></i><span>{{ __('massages.Dashboard') }}</span></a>
        </li>

        <li class="menu-header"  style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Management') }}</li>
            {{-- <li>
                    <a href="#" class="has-dropdown"><i class="ion ion-person"></i>
                        <span>Users</span></a>
                    <ul class="menu-dropdown">
                    <li class="active"><a href=" {{route('admin.dashboard')}}"><i class="ion ion-ios-circle-outline"></i>User</a></li>
                    <li class="{{ request()->is('/admin/dashboard')? 'active' : '' }}"><a href="fontawesome.html"><i class="ion ion-ios-circle-outline"></i>Role</a></li>
                    <li class="{{ request()->is('/admin/dashboard')? 'active' : '' }}"><a href="flag.html"><i class="ion ion-ios-circle-outline"></i>Permision</a></li>
                    <li class="{{ request()->is('/admin/dashboard')? 'active' : '' }}"><a href="flag.html"><i class="ion ion-ios-circle-outline"></i>Add New</a></li>

                    </ul>
          </li> --}}

          {{-- <li class="{{ request()->is( 'admin/employee')? 'active' : '' }}">
            <a href="#" class="has-dropdown"><i class="fas fa-users"></i><span>Employee</span></a>
            <ul class="menu-dropdown"> --}}
              {{-- <li><a href="flag.html"><i class="ion ion-ios-circle-outline"></i></a></li> --}}
              {{-- <li><a href="{{ request()->is( 'admin/employee/contact')? 'active' : '' }}"><i class="ion ion-ios-circle-outline"></i>Contact list</a></li> --}}

            {{-- </ul> --}}
          {{-- </li> --}}

          <li class="{{ request()->is( 'admin/department')? 'active' : '' }}">
            <a href="{{
           route('department.index')
            }}">
            <i class="far fa-building"></i><span>{{ __('massages.Department') }}</span></a>
          </li>
          <li class="{{ request()->is('admin/position')? 'active' : '' }}">
            <a href="{{
           route('position.index')
            }}">
            <i class="fas fa-book"></i><span>{{ __('massages.Position') }}</span></a>
          </li>
          <li class="{{ request()->is( 'admin/employee')? 'active' : '' }}">
            <a href="
          {{ route('employee.index') }}
            ">
            <i class="fas fa-users"></i><span>{{ __('massages.Employee') }}</span></a>
          </li>
        {{-- <li>
          <a href="#" class="has-dropdown"><i class="far fa-building"></i>
            <span>Department</span></a>
          <ul class="menu-dropdown">
            <li><a href="{{ route('department.index') }}"><i class="ion ion-ios-circle-outline"></i>Department</a></li>
            <li><a href="{{ route('position.index') }}"><i class="ion ion-ios-circle-outline"></i>Position</a></li>
          </ul>
        </li> --}}

      {{-- <li class="">
        <a href="" class="ml-2">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cash-coin mr-4" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M11 15a4 4 0 1 0 0-8 4 4 0 0 0 0 8zm5-4a5 5 0 1 1-10 0 5 5 0 0 1 10 0z"/>
                <path d="M9.438 11.944c.047.596.518 1.06 1.363 1.116v.44h.375v-.443c.875-.061 1.386-.529 1.386-1.207 0-.618-.39-.936-1.09-1.1l-.296-.07v-1.2c.376.043.614.248.671.532h.658c-.047-.575-.54-1.024-1.329-1.073V8.5h-.375v.45c-.747.073-1.255.522-1.255 1.158 0 .562.378.92 1.007 1.066l.248.061v1.272c-.384-.058-.639-.27-.696-.563h-.668zm1.36-1.354c-.369-.085-.569-.26-.569-.522 0-.294.216-.514.572-.578v1.1h-.003zm.432.746c.449.104.655.272.655.569 0 .339-.257.571-.709.614v-1.195l.054.012z"/>
                <path d="M1 0a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h4.083c.058-.344.145-.678.258-1H3a2 2 0 0 0-2-2V3a2 2 0 0 0 2-2h10a2 2 0 0 0 2 2v3.528c.38.34.717.728 1 1.154V1a1 1 0 0 0-1-1H1z"/>
                <path d="M9.998 5.083 10 5a2 2 0 1 0-3.132 1.65 5.982 5.982 0 0 1 3.13-1.567z"/>
              </svg>
                <span>Salary</span>
            </a>
      </li> --}}

        <li class="menu-header"  style="font-family: 'Noto Serif Khmer', serif;">Contact & Notification</li>
        {{-- <li class="{{ request()->is( 'admin/employee')? 'active' : '' }}">
            <a href="
          {{ route('employee.index') }}
            ">
            <i class="ion ion-paper-airplane"></i><span>Massager</span></a>
          </li>
 --}}

    </aside>
  </div>
