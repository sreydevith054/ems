    <section class="section">
      <h1 class="section-header">
        <div style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Dashboard') }}</div>
      </h1>
      <div class="row d-flex justify-content-center">
        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-primary">
              <i class="ion ion-person"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.TOTAL USERS') }}</h4>
              </div>
              <div class="card-body">
                {{$users}}
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-danger">
                <i class="fas fa-users"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.EMPLOYEE') }}</h4>
              </div>
              <div class="card-body">
                {{ $employee }}
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-12">
          <div class="card card-sm-3">
            <div class="card-icon bg-success">
                <i class="far fa-building"></i>
            </div>
            <div class="card-wrap">
              <div class="card-header">
                <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.DEPARTMENT') }}</h4>
              </div>
              <div class="card-body">
                {{ $department }}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
          <div class="card">
            <div class="card-header">
              <div class="float-right">
                <nav class="d-inline-block">
                    {{ $panel->links() }}

                  </nav>
                 </div>
              <h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.USERS PANNEL') }}</h4>
            </div>
            <div class="card-body text-center">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                        <th>{{ __('massages.ID') }}</th>
                        <th>{{ __('massages.Name') }}</th>
                        {{-- <th>Kh Name</th> --}}
                        <th>{{ __('massages.Role') }}</th>
                        <th>{{ __('massages.Contact') }}</th>
                </tr>
                  </thead>
                  <tbody>
                      @foreach ($panel as $panels)
                      <tr>
                        <td>
                          {{ $panels['id'] }}
                        </td>
                        <td>
                          <a href="{{ url('admin/profile') }}"><img src="{{ asset('storage/'.$panels->profile_photo_path) }}" alt="{{ $panels->name }}" width="50" class="rounded-circle img-thumbnail mr-1">{{ $panels['name'] }}</a>
                        </td>
                        {{-- <td>
                            {{ $panels['khName'] }}
                        </td> --}}
                        <td>
                            {{ __('massages.'.$panels['user_type']) }}
                          </td>
                          <td>
                            {{ $panels['email'] }}<br>{{ $panels['phone'] }}
                          </td>


                        {{-- <td>
                          <a class="btn btn-success btn-action" data-toggle="tooltip" title="view"><i class="fas fa-eye"></i></a>
                          <a class="btn btn-primary btn-action mr-1" data-toggle="tooltip" title="Edit"><i class="ion ion-edit"></i></a>
                          <a class="btn btn-danger btn-action" data-toggle="tooltip" title="Delete"><i class="ion ion-trash-b"></i></a>
                        </td> --}}
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    {{-- <h1>@lang("Dashboard")</h1> --}}
