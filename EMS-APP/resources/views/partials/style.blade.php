<link rel="stylesheet" href="{{ URL::to('../dist/modules/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/ionicons/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">

<link rel="stylesheet" href="{{ URL::to('../dist/modules/summernote/summernote-lite.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/modules/flag-icon-css/css/flag-icon.min.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/css/demo.css') }}">
<link rel="stylesheet" href="{{ URL::to('../dist/css/style.css') }}">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Serif+Khmer:wght@300&display=swap" rel="stylesheet">
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> --}}
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.2.0/css/fixedHeader.bootstrap4.min.css">

<style>
    body{
        font-family: 'Noto Serif Khmer', serif;

    }
    .input-group-text, select.form-control:not([size]):not([multiple]), .form-control:not(.form-control-sm):not(.form-control-lg) {
    font-size: 14px;
    padding: 5px 23px;
    line-height: 24px;
}</style>
