{{-- <script src="{{ URL::to('../dist/modules/jquery.min.js') }}"></script> --}}
<script src="{{ URL::to('../dist/modules/popper.js') }}"></script>
<script src="{{ URL::to('../dist/modules/tooltip.js') }}"></script>
<script src="{{ URL::to('../dist/modules/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::to('../dist/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ URL::to('../dist/modules/scroll-up-bar/dist/scroll-up-bar.min.js') }}"></script>
<script src="{{ URL::to('../dist/js/sa-functions.js') }}"></script>

<script src="{{ URL::to('../dist/modules/chart.min.js') }}"></script>
<script src="{{ URL::to('../dist/modules/summernote/summernote-lite.js') }}"></script>

{{-- <script>
    URL = "{{ route('changelang') }}";

    $("#changelang").change(function(){
        window.location.href = URL + "?lang="+ $(this).val();
    });

</script> --}}
<script src="{{ URL::to('../dist/js/scripts.js') }}"></script>
<script src="{{ URL::to('../dist/js/custom.js') }}"></script>
{{-- <script src="{{ URL::to('../dist/js/demo.js') }}"></script> --}}
