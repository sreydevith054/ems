{{-- <x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div>
                <x-jet-label for="username" value="{{ __('Username') }}" />
                <x-jet-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username')" required />
            </div>
            <div class="mt-4">
                <x-jet-label for="phone" value="{{ __('Phone Number') }}" />
                <x-jet-input id="phone" class="block mt-1 w-full" type="text" name="phone" :value="old('phone')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-jet-label for="terms">
                        <div class="flex items-center">
                            <x-jet-checkbox name="terms" id="terms"/>

                            <div class="ml-2">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-jet-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Register') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout> --}}
{{-- @extends('layouts.base') --}}
<x-guest-layout>

@section('title','register')

{{-- @section('content') --}}
<div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2 mt-5">
            <div class="login-brand mb-5">

            </div>

            <div class="card card-primary m-5">
              <div class="card-header"><h4 style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Register') }}</h4></div>

              <div class="card-body">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                          <div class="row">
                    <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;" for="name">{{ __('massages.Name') }}</label>
                      <input id="name" type="text" class="form-control" name="name" :value="old('name')" required autofocus autocomplete="name">
                    </div>
                    {{-- <div class="form-group col-6">
                        <label style="font-family: 'Noto Serif Khmer', serif;" for="KhName">{{ __('massages.KhName') }}</label>
                        <input id="KhName" type="text" class="form-control" name="KhName" :value="old('KhName')" required autofocus autocomplete="KhName">
                      </div> --}}


                    <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;" for="username">{{ __('massages.username') }}</label>
                      <input id="username" type="text" class="form-control"
                      name="username" :value="old('username')" required>
                    </div>
                    {{-- <div class="form-group col-6">
                        <label style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Phone Number') }}</label>
                        <input type="tel" class="form-control" name="phone" :value="old('phone')" required>
                      </div> --}}

                  </div>

                  <div class="form-group ">
                    <label style="font-family: 'Noto Serif Khmer', serif;" for="email">{{ __('massages.Email') }}</label>
                    <input id="email" type="email" class="form-control" name="email"
                    name="email" :value="old('email')" required
                    >
                    <div class="invalid-feedback">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;" for="password" class="d-block">{{ __('massages.Password') }}</label>
                      <input id="password" type="password" class="form-control"
                      name="password" required autocomplete="new-password"
                      >
                    </div>
                    <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;" for="password_confirmation" class="d-block">{{ __('massages.Password Confirmation') }}</label>
                      <input id="password_confirmation" type="password" class="form-control"
                      name="password_confirmation" required autocomplete="new-password"
                     >
                    </div>
                  </div>

                  {{-- <div class="form-divider">
                   Details
                  </div> --}}
                  {{-- <div class="row"> --}}
                    {{-- <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;">Country</label>
                      <select class="form-control">
                        <option>Indonesia</option>
                        <option>Palestine</option>
                        <option>Syria</option>
                        <option>Malaysia</option>
                        <option>Thailand</option>
                      </select>
                    </div> --}}
                    {{-- <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;">Province</label>
                      <select class="form-control">
                        <option>West Java</option>
                        <option>East Java</option>
                      </select>
                    </div> --}}
                  {{-- </div> --}}
                  <div class="row">
                    <div class="form-group col-6">
                      <label style="font-family: 'Noto Serif Khmer', serif;">{{ __('massages.Phone Number') }}</label>
                      <input type="tel" class="form-control" name="phone" :value="old('phone')" required>
                    </div>
                    <div class="form-group col-6">
                      {{-- <label style="font-family: 'Noto Serif Khmer', serif;"></label> --}}
                      {{-- <input type="text" class="form-control"> --}}
                    </div>
                  </div>

                  <div class="form-group">
                    {{-- <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="agree" class="custom-control-input" id="agree">
                      <label style="font-family: 'Noto Serif Khmer', serif;" class="custom-control-label" for="agree">I agree with the terms and conditions</label>
                    </div> --}}
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                      {{ __('massages.Register') }}
                    </button>
                  </div>
                </form>
              </div>
            </div>
            {{-- <div class="simple-footer">
              Copyright &copy; Stisla 2018
            </div> --}}
          </div>
        </div>
      </div>
    </section>
  </div>
</x-guest-layout>
