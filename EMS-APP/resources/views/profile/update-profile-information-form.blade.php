{{-- <x-jet-form-section submit="updateProfileInformation"> --}}
    {{-- <x-slot name="title">
        {{ __('Profile Information') }}
    </x-slot> --}}
    {{-- <x-slot name="description">
        {{ __('Update your account\'s profile information and email address.') }}
    </x-slot> --}}

    {{-- <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                            " />

                <x-jet-label for="photo" value="{{ __('Photo') }}" />

                <!-- Current Profile Photo -->
                <div class="mt-2" x-show="! photoPreview">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div>

                <!-- New Profile Photo Preview -->
                <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20 bg-cover bg-no-repeat bg-center"
                          x-bind:style="'background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div>

                <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-jet-secondary-button>

                @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-jet-secondary-button>
                @endif

                <x-jet-input-error for="photo" class="mt-2" />
            </div>
        @endif

        <!-- Name -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="name" value="{{ __('Name') }}" />
            <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="state.name" autocomplete="name" />
            <x-jet-input-error for="name" class="mt-2" />
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" value="{{ __('Email') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
    </x-slot> --}}

    {{-- <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Save') }}
        </x-jet-button>
    </x-slot> --}}
{{-- </x-jet-form-section> --}}
<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{-- {{ __('Profile Information') }} --}}
    </x-slot>
    <x-slot name="description">
        {{-- {{ __('Update your account\'s profile information and email address.') }} --}}
    </x-slot>

    <x-slot name="form">
        <!-- Profile Photo -->
        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <div x-data="{photoName: null, photoPreview: null}" class="col-span-6 sm:col-span-4">
                <!-- Profile Photo File Input -->
                {{-- <input type="file" class="hidden"
                            wire:model="photo"
                            x-ref="photo"
                            x-on:change="
                                    photoName = $refs.photo.files[0].name;
                                    const reader = new FileReader();
                                    reader.onload = (e) => {
                                        photoPreview = e.target.result;
                                    };
                                    reader.readAsDataURL($refs.photo.files[0]);
                        " /> --}}
                <div class="col-xs-9 col-sm-9 col-md-9 mb-2">
                    <strong>{{ __('massages.Image') }}:</strong>
                    <div class="custom-file ">
                        <input type="file"
                                wire:model="photo"
                                x-ref="photo"
                                x-on:change="
                                photoName = $refs.photo.files[0].name;
                                const reader = new FileReader();
                                reader.onload = (e) => {
                                    photoPreview = e.target.result;
                                };
                                reader.readAsDataURL($refs.photo.files[0]);"
                                class="custom-file-input"
                                id="chooseFile"
                                >
                    <x-jet-label for="photo" class="custom-file-label" value="{{ __('massages.Select Photo') }}" />
                </div>

            </div>


                {{-- <x-jet-label for="photo" class="custom-file-label" value="{{ __('Photo') }}" /> --}}

                <!-- Current Profile Photo -->
                {{-- <div class="mt-2">
                    <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}" class="rounded-full h-20 w-20 object-cover">
                </div> --}}
                <!-- New Profile Photo Preview -->
                {{-- <div class="mt-2" x-show="photoPreview">
                    <span class="block rounded-full w-20 h-20 bg-cover bg-no-repeat bg-center"
                          x-bind:style="'background-image: url(\'' + photoPreview + '\');'">
                    </span>
                </div> --}}

                {{-- <x-jet-secondary-button class="mt-2 mr-2" type="button" x-on:click.prevent="$refs.photo.click()">
                    {{ __('Select A New Photo') }}
                </x-jet-secondary-button> --}}

                {{-- @if ($this->user->profile_photo_path)
                    <x-jet-secondary-button type="button" class="mt-2" wire:click="deleteProfilePhoto">
                        {{ __('Remove Photo') }}
                    </x-jet-secondary-button>
                @endif --}}

                {{-- <x-jet-input-error for="photo" class="mt-2" /> --}}
            </div>
        @endif

        <!-- Name -->
        <div class="col-xs-9 col-sm-9 col-md-9">
            <div class="form-group">
                <strong>{{ __('massages.Name') }}:</strong>
                <x-jet-input id="name" type="text" class="form-control" placeholder="{{ __('massages.Name') }}" wire:model.defer="state.name" autocomplete="name" />
                <x-jet-input-error for="name" class="mt-2" />
            </div>
        </div>

        <!-- Email -->
        <div class="col-xs-9 col-sm-9 col-md-9">
            <div class="form-group">
                <strong>{{ __('massages.Email') }}:</strong>
                <x-jet-input id="name" type="email" class="form-control" placeholder="{{ __('massages.Email') }}" wire:model.defer="state.email" />
                <x-jet-input-error for="name" class="mt-2" />
            </div>
        </div>

    </x-slot>
    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Saved.') }}
        </x-jet-action-message>
        <a href="{{ route('admin.dashboard') }}" class="btn btn-danger float-left" role="button" data-toggle="tooltip" title="" data-original-title="Back"><i class="ion-arrow-left-a"></i></a>

        <x-jet-button class="btn btn-primary btn-sm" wire:loading.attr="disabled" wire:target="photo">
            <i class="ion-checkmark-circled"></i>
        </x-jet-button>

    </x-slot>
</x-jet-form-section>
